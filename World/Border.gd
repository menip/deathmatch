extends NinePatchRect

# Add some juice when on border :D
func hit_border():
	$Tween.stop_all()
	$Tween.interpolate_property(self, "modulate", Color(1,.5,.5), Color.white, 0.3, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()