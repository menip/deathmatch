extends Camera2D

var target = null

onready var tween_rebound = $TweenRebound
var rebound = Vector2()

func _process(delta):
	if target != null:
		position = target.position
	
	
#	if rebound != null:
#		offset += (3 * delta) * rebound # Rebound back in 1/3 second
#
#		if (offset - rebound).length_squared() < 50:
#			rebound = null
#	elif offset.length_squared() > 100: # Mostly arbitrary 10
#		offset -= (delta) * offset # Rebound forward in a second


func bullet_fire():
	var scale_effect = 5
	# Could just create vec with desired magnitude and rotate it by rot
#	if rebound == null:
#		rebound = Vector2(scale_effect * cos(rot - PI/2), scale_effect * sin(rot - PI/2))
#	else:
	var pos = target.position
	var rot = target.rotation - PI/2

	rebound += Vector2(scale_effect * cos(rot), scale_effect * sin(rot))
	if tween_rebound.is_active():
		tween_rebound.stop(self, "offset")
	tween_rebound.interpolate_property(self, "offset", offset, rebound, .1, Tween.TRANS_EXPO, Tween.EASE_IN)
	tween_rebound.start()


func connect_to_target(_target):
	target = _target


func disconnect_from_target():
	target = null

func _on_rebound_completed(object, key):
	if object == self and key == ":offset":
		rebound = Vector2()
		
		tween_rebound.interpolate_property(self, "offset", offset, Vector2(), .4, Tween.TRANS_EXPO, Tween.EASE_OUT)
		tween_rebound.start()
