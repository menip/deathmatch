extends Control

signal options_pressed()
signal credits_pressed()

onready var play:Button = $HBox/VBox/Buttons/Play
onready var status:Label = $HBox/VBox/PanelContainer/VBox/Status
onready var players:Label = $HBox/VBox/PanelContainer/VBox/Players
onready var bots:Label = $HBox/VBox/PanelContainer/VBox/Bots


func _ready():
	gamestate.connect("connection_failed", self, "_on_connection_failed")
	gamestate.connect("connection_succeeded", self, "_on_connection_success")
	gamestate.connect("server_disconnected", self, "_on_server_disconnect")
	gamestate.connect("game_ended", self, "_on_game_ended")
	
	play.disabled = true
	status.text = "Connecting..."
	status.modulate = Color.yellow


func _on_Play_pressed():
	gamestate.pre_start_game()


func _on_Options_pressed():
	emit_signal("options_pressed")


func _on_Credits_pressed():
	emit_signal("credits_pressed")

func _on_connection_success():
	play.disabled = false
	status.text = "Connected"
	status.modulate = Color.green


func _on_connection_failed():
	play.disabled = true
	
	status.text = "Connection Failed, trying again"
	status.modulate = Color.red

func _on_server_disconnect():
	play.disabled = true
	
	status.text = "Server Disconnected, trying to connect..."
	status.modulate = Color.red


func _on_game_ended():
	show()


# Bad way to do this, esp as # of player scale. Should have timer server side probs
func _on_CountsUpdateTimer_timeout():
	players.text = "Players: " + String(gamestate.player_count)
	bots.text = "Bots: " + String(gamestate.bot_count)
	gamestate.rpc_id(1, "combatant_count")

