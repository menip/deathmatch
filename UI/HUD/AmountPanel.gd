extends PanelContainer

func update_amount(amount, max_amount):
	$HBoxContainer/ProgressBar.max_value = max_amount
	$HBoxContainer/ProgressBar.value = amount