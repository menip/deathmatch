extends MarginContainer

var ability_id = -1

onready var fire = $HBox/VBoxGun/Fire
onready var ammo = $HBox/VBoxGun/Ammo
onready var ability = $HBox/AbilityRecharge
onready var shield = $HBox/VBoxHealth/Shield
onready var health = $HBox/VBoxHealth/Health
onready var tween = $Tween

signal ability_press()


func set_ability(_ability_id):
	ability_id = _ability_id
	
	var icon = ResourceLoader.load(globals.data["abilities"][_ability_id]["icon"])
	ability.get_node("HBoxContainer/Ability").texture_normal = icon


func update_health(_health, max_health):
	health.update_amount(_health, max_health)


func shield_health_changed(_health, max_health):
	shield.update_amount(_health, max_health)


func ammo_changed(_ammo, max_ammo):
	ammo.update_amount(_ammo, max_ammo)


func fire_changed(time_left, total_time):
	fire.update_amount(time_left, total_time)


func ability_changed(time_left, total_time):
	ability.get_node("HBoxContainer/VBoxContainer/ProgressBar").max_value = total_time
	ability.get_node("HBoxContainer/VBoxContainer/ProgressBar").value = time_left


func _on_Ability_pressed():
	emit_signal("ability_press") 


func on_ready_ability():
	blink(ability, Color(.5, 1, .5, 1), .7)


func on_not_ready_ability():
	blink(ability, Color(1, .5, .5, 1), .5)


func on_not_ready_fire():
	blink(fire, Color(1, .5, .5, 1), .5)


func on_not_enough_ammo():
	blink(ammo, Color(1, .5, .5, 1), .5)


func blink(node, blink_color, duration):
	tween.interpolate_property(node, "modulate", Color.white, blink_color, duration/2, Tween.TRANS_CIRC, Tween.EASE_OUT)
	tween.interpolate_property(node, "modulate", blink_color, Color.white, duration/2, Tween.TRANS_CIRC, Tween.EASE_OUT, duration/2)
	tween.start()

