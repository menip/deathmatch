extends Control

onready var weapons = get_node("MC/HBox/VBox/Selections/Weapons")
onready var shields = get_node("MC/HBox/VBox/Selections/Shields")
onready var abilities = get_node("MC/HBox/VBox/Selections/Abilities")

onready var tween = $Tween

var ItemInfo = preload("res://UI/PlayerSetup/ItemInfo.tscn")


func _ready():
	setup_selections()


func setup_selections():
	weapons.set_title("Select Weapon")
	shields.set_title("Select Shield")
	abilities.set_title("Select Ability")
	
	setup_selection(weapons, "weapons")
	setup_selection(shields, "shields")
	setup_selection(abilities, "abilities")
	
	emit_signal("minimum_size_changed")


func show_select():
	show()


func _on_ReadyButton_pressed():
	# Where is finally when you need it :/
	var failed_to_select = false
	if not weapons.is_anything_selected():
		blink(weapons, Color(1, .5, .5, 1), .5)
		failed_to_select = true
	if not shields.is_anything_selected():
		blink(shields, Color(1, .5, .5, 1), .5)
		failed_to_select = true
	if not abilities.is_anything_selected():
		blink(abilities, Color(1, .5, .5, 1), .5)
		failed_to_select = true
	if failed_to_select: 
		return
	
	var id = get_tree().get_network_unique_id()
	
	var player_name = $MC/HBox/VBox/PlayerName/NameInput.text
	var gun_id = weapons.get_selected_type()
	var shield_id = shields.get_selected_type()
	var ability_id = abilities.get_selected_type()
	
	gamestate.rpc_id(1,"spawn_player", player_name, gun_id, shield_id, ability_id) # Ask server to spawn player
	hide()


func setup_selection(selection:Control, name:String):
	for i in globals.data[name]:
		var item = ItemInfo.instance()
		
		item.identifier = i
		item.set_title(globals.data[name][i]["name"])
		item.set_icon(ResourceLoader.load(globals.data[name][i]["icon"]))
		item.set_description(globals.data[name][i]["description"])
		
		# Could change dict to have properties dict and iterate over that, but don't wanna do that now
		if globals.data[name][i].has("bullets"):
			item.add_property("Shots", String(globals.data[name][i]["bullets"]))
		if globals.data[name][i].has("fire_rate"):
			item.add_property("Fire Rate", String(globals.data[name][i]["fire_rate"]))
		if globals.data[name][i].has("bullet_speed"):
			item.add_property("Speed", String(globals.data[name][i]["bullet_speed"]))
		if globals.data[name][i].has("health"):
			item.add_property("Health", String(globals.data[name][i]["health"]))
		if globals.data[name][i].has("regen_rate"):
			item.add_property("Regen Rate", String(globals.data[name][i]["regen_rate"]))
		if globals.data[name][i].has("cooldown"):
			item.add_property("Cooldown", String(globals.data[name][i]["cooldown"]))
		if globals.data[name][i].has("range"):
			item.add_property("Range", String(globals.data[name][i]["range"]))
		if globals.data[name][i].has("percent_move_speed_bonus"):
			item.add_property("Speed Bonus", String(globals.data[name][i]["percent_move_speed_bonus"]) + "%")
		
		selection.add_item(item)


func blink(node, blink_color, duration):
	tween.interpolate_property(node, "modulate", Color.white, blink_color, duration/2, Tween.TRANS_CIRC, Tween.EASE_OUT)
	tween.interpolate_property(node, "modulate", blink_color, Color.white, duration/2, Tween.TRANS_CIRC, Tween.EASE_OUT, duration/2)
	tween.start()