extends Area2D
class_name Shield

var MAX_HEALTH
var RECHARGE_RATE
var health

signal health_changed(new_health)


puppet func shield_change(new_amount):
	health = new_amount
	update_aliveness()


func update_aliveness():
	$Sprite.modulate = Color.white.linear_interpolate(Color.blue, 1 - health/MAX_HEALTH) 

	if health <= 0:
		hide()
		$CollisionShape2D.set_deferred("disabled", true)
	else:
		show()
		$CollisionShape2D.set_deferred("disabled", false)
	
	emit_signal("health_changed", health, MAX_HEALTH)

