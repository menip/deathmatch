extends Node

var node_world = null
var node_HUD = null
var node_Leaderboard = null
var node_PlayerSelect = null
var node_Players = null
var node_bullet_container = null
var node_Announcer = null


var BOUNDS

var data:Dictionary = {}
var save_location = "user://settings_v0.1.0.json"

func _ready():
	if not OS.is_debug_build():
		load_settings()

puppet func set_data(_data):
	print("Received data")
	data = _data
	BOUNDS = Rect2(0,0, data["play_size"]["x"], data["play_size"]["y"])


func setup_refs():
	node_world = get_tree().get_root().get_node("World")
	node_HUD = node_world.get_node("GUI/HUD")
	node_Leaderboard = node_world.get_node("GUI/Leaderboard")
	node_PlayerSelect = node_world.get_node("GUI/PlayerSetup")
	node_Players = node_world.get_node("Players")
	node_bullet_container = node_world.get_node("Bullets")
	node_Announcer = node_world.get_node("GUI/Announcer")


func save_settings():
	var settings = {"ability":[], "view_leaderboard":[]}
	for action in settings:
		for event in InputMap.get_action_list(action):
			if event is InputEventKey:
				var event_info = {"scancode": event.scancode, "alt": event.alt, "shift":event.shift, "control":event.control, "meta": event.meta, "command":event.command}
				settings[action].append(event_info)
	
	write_json_data(settings, save_location)


func load_settings():
	var settings = load_json_data(save_location)
	
	for action in settings:
		for event_info in settings[action]:
			var event = InputEventKey.new()
			event.scancode = event_info["scancode"]
			event.alt = event_info["alt"]
			event.shift = event_info["shift"]
			event.control = event_info["control"]
			event.meta = event_info["meta"]
			event.command = event_info["command"]
			
			# Note that currently only events that are key presses can be loaded
			InputMap.action_erase_events(action)
			InputMap.action_add_event(action, event)


func write_json_data(data, path):
	var file = File.new()
	
	assert(path != null)
	file.open(path, File.WRITE)
	file.store_string(to_json(data))
	file.close()


func load_json_data(path):
	var file = File.new()
	
	if path == null:
		return {}
	if !file.file_exists(path):
		return {}
	
	file.open(path, File.READ)
	var data = {}
	data = parse_json(file.get_as_text())
	file.close()
	return data