extends Node

# Game port and ip
const ip = "127.0.0.1"
const DEFAULT_PORT = 55555

# Max number of players
const MAX_PEERS = 12

# Name for my player
var player_name = "Combatant"

# For pinging. Note in godot framerate tied to networking
signal latency_update()
var timer_ping_start:float
var latency:float
var ping_timer:Timer

# To know player/bot count in main menu
puppet var player_count:int = 0
puppet var bot_count:int = 0

# TODO: Should this be kept in gamestate or moved to World?
puppet var players = {}

# Signals to let lobby GUI know what's going on
signal connection_failed()
signal connection_succeeded()
signal server_disconnected()
signal game_ended()
signal game_error(what)


func _ready():
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	
	# Attempt to connect to server immediatelly
	connect_to_server()
	
	# Setup ping timer
	ping_timer = Timer.new()
	ping_timer.connect("timeout", self, "ping")
	ping_timer.wait_time = 1
	ping_timer.autostart = true
	add_child(ping_timer)


func connect_to_server():
	var host = NetworkedMultiplayerENet.new()
	host.create_client(ip, DEFAULT_PORT)
	get_tree().set_network_peer(host)


# Callback from SceneTree, only for clients (not server)
func _connected_ok():
	# Registration of a client beings here, tell server that we are here
	
	emit_signal("connection_succeeded")


# Callback from SceneTree, only for clients (not server)
func _server_disconnected():
	emit_signal("game_error", "Server disconnected")
	emit_signal("server_disconnected")
	end_game()
	
	# Try to connect again
	connect_to_server()


# Callback from SceneTree, only for clients (not server)
func _connected_fail():
	get_tree().set_network_peer(null) # Remove peer
	emit_signal("connection_failed")
	
	# Try to connect again
	connect_to_server()


# Lobby management functions
puppet func register_player(id, new_player_data):
	players[id] = new_player_data


puppet func unregister_player(id):
	players.erase(id)


puppet func pre_start_game():
	# Change scene
	var world = load("res://World/World.tscn").instance()
	get_tree().get_root().add_child(world)
	
	globals.setup_refs()
	
	get_tree().get_root().get_node("StartScreen").hide()

	# Tell server we are ready to start
	rpc_id(1, "ready_to_start")


puppet func post_start_game():
	# TODO: is this even needed?
	get_tree().set_pause(false) # Unpause and unleash the game!


# Returns list of player names
func get_player_list():
	var names = []
	for id in players:
		names.append(players[id]["name"])
	
	return names


# TODO: probs need to be remote, but clients can also call without rpc
func end_game():
	if has_node("/root/World"): # Game is in progress
		# End it
		get_node("/root/World").queue_free()

	emit_signal("game_ended")
	players.clear()
	get_tree().set_network_peer(null) # End networking


puppet func add_kill(id_killer, id_killed):
	players[id_killer]["total_kills"] += 1
	players[id_killer]["kill_streak"] += 1
	
	players[id_killed]["kill_streak"] = 0
	
	if get_tree().get_root().has_node("World"):
		globals.node_Announcer.announce(players[id_killer]["name"] + " pawned " + players[id_killed]["name"])


func exit_game():
	globals.node_world.call_deferred("queue_free")
	get_node("/root/StartScreen").show()
	
	players.clear()
	
	rpc_id(1, "player_exit_game")


func ping():
	timer_ping_start = OS.get_ticks_msec()
	rpc_id(1, "ping")


puppet func pong():
	latency = OS.get_ticks_msec() - timer_ping_start
	emit_signal("latency_update")





