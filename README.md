### 2D Networked Deathmatch

# Into
This project came about as a way to learn networking in the Godot game engine. As such, there are plenty examples of how not to do things :P

At this point, all major ideas are implemented. There is still a lot of work to be done, and I expect to set aside another time block to work on deathmatch in coming months. PRs are welcome in future, but for now there are several places I want to refactor first.

# General concept
Select guns and armor type in lobby.

During gameplay, can pick up randomly spawned health and shield regen, as well as ammo.  

Shoot people and kill them.

Killboard holds most kills.

# Minimum Viable Product now complete

# Running
### Try online
Go to https://menip.itch.io/deathmatch to download ready game. Unzip, run, and if connection to server is established, you can play :)

### Locally
Download project and open with [Godot 3.1.1](https://godotengine.org/download/).  
To test as single client, just run inside editor.  
To test multiple clients you can export. Inside editor, go Project>Export. Make sure export templates are installed, select your desired preset, and export project. Please keep binaries in bin/ so as to not clutter git. Once exported, open multiple instances of the binary. You'll need to start up server from https://gitlab.com/menip/deathmatch-server.

# Future
Issue tracker is best represantion of what I want to do with this project. Major items are adding animations and audio (yay for alliterations :D ). Beyond that is a refactor, and lots of bugfixing and polish. 
