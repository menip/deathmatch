extends Area2D

var direction = Vector2()

var id_origin = -1

var gun = null

var laser_size


func _ready():
	# Really need to do the whole bots/players diff
	gun = globals.node_Players.get_node("player_" + String(id_origin) + "/GunAnchor/Gun")
	if gun == null:
		gun = globals.node_Players.get_node("bot_" + String(id_origin) + "/GunAnchor/Gun")
	
	gun.connect_laser(self)
	resize(laser_size)



func _process(delta):
	position = gun.global_position
	rotation = gun.global_rotation


func set_id_origin(id):
	id_origin = id


puppet func remove_bullet():
	queue_free()


puppet func resize(_size):
	$Sprite.rect_size.y = $Sprite.texture.get_size().y * _size
	$CollisionShape2D.shape.extents.y = $Sprite.texture.get_size().y * _size / 2
	$CollisionShape2D.position.y = -1 * $Sprite.texture.get_size().y * _size / 2