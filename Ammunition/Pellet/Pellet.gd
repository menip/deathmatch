extends Area2D

var BULLET_SPEED
var direction = Vector2()

var id_origin = -1


func set_id_origin(id):
	id_origin = id


func _ready():
	direction = Vector2(cos(rotation - PI/2), sin(rotation - PI/2))


func _process(delta):
	position += direction * BULLET_SPEED * delta


puppet func remove_bullet():
	queue_free()