extends Sprite

class_name Gun # TODO: add , "path/to/clas/image"

var MAX_BULLETS

var id_player = -1
var bullet_count = -1

export(int) var ammo_id = -1

var is_laser = false # hack

signal ammo_changed(ammo, max_ammo)
signal refresh()
signal rearm()


puppet func ammo_change(new_amount):
	bullet_count = new_amount
	emit_signal("ammo_changed", bullet_count, MAX_BULLETS)


puppet func rearm():
	emit_signal("rearm")


puppet func refresh():
	emit_signal("refresh")

