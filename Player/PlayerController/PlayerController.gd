extends Node2D

var player = null
var gun = null
var gun_anchor = null
var shield = null
var ability_id = "-1"

var can_fire = false
var is_firing = false

var awaiting_telleport_click = false

signal ready_ability
signal not_ready_ability
signal not_ready_fire
signal not_enough_ammo


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if player != null:
		position = player.position
		
		# Player Process
		var move_dir = Vector2()
		
		if Input.is_action_pressed("move_up"):
			move_dir.y = -1
		if Input.is_action_pressed("move_down"):
			move_dir.y = 1
		if Input.is_action_pressed("move_left"):
			move_dir.x = -1
		if Input.is_action_pressed("move_right"):
			move_dir.x = 1
		
		gun_anchor.look_at(get_global_mouse_position())
		gun_anchor.rotation += PI/2
		player.velocity = move_dir.normalized() * player.SPEED_MOVE
		
		player.rpc_id(1, "update_vals", move_dir, gun_anchor.rotation)
		
		globals.node_HUD.ability_changed($AbilityCooldown.wait_time - $AbilityCooldown.time_left, $AbilityCooldown.wait_time)
		
		# Gun Process
		globals.node_HUD.fire_changed($FireDelay.wait_time - $FireDelay.time_left, $FireDelay.wait_time)
		
		if not gun.is_laser and can_fire and gun.bullet_count > 0 and is_firing:
			gun.rpc_id(1, "fire")
			# Camera shake feels bad atm
			#globals.node_world.get_node("Camera2D").bullet_fire()


func _unhandled_input(event):
	if player != null:
		# Player
		# Handle abilities
		if not awaiting_telleport_click:
			if Input.is_action_just_pressed("ability"):
				if $AbilityCooldown.is_stopped():
					if ability_id == "1":
						awaiting_telleport_click = true
						update() # So circle drawn
					else:
						player.rpc_id(1, "do_ability")
				else:
					emit_signal("not_ready_ability")
		# Handle telleport
		elif Input.is_action_just_pressed("fire_primary") or Input.is_action_just_pressed("ability"):
			awaiting_telleport_click = false
			player.rpc_id(1, "do_ability", get_global_mouse_position())
		
		# Gun
		if Input.is_action_just_pressed("fire_primary"):
			if not can_fire:
				emit_signal("not_ready_fire")
			if not gun.bullet_count > 0:
				emit_signal("not_enough_ammo")
			is_firing = true
			
			# Hack, if we are laser:
			if gun.is_laser and can_fire and gun.bullet_count > 0:
				gun.rpc_id(1, "fire")
		elif Input.is_action_just_released("fire_primary"):
			is_firing = false
			
			if gun.is_laser:
				gun.rpc_id(1, "turn_off_laser")


func set_player(_player):
	player = _player
	controller_init()


func controller_init():
	# Setup refs
	gun_anchor = player.get_node("GunAnchor")
	gun = gun_anchor.get_node("Gun")
	shield = player.get_node("Shield")
	
	# Init HUD
	globals.node_HUD.show() # Make HUD visible once player is spawned 
	globals.node_world.get_node("Camera2D").connect_to_target(player)
	globals.node_HUD.set_ability(ability_id)
	globals.node_HUD.connect("ability_press", self, "_on_ability_pressed")
	connect("ready_ability", globals.node_HUD, "on_ready_ability")
	connect("not_ready_ability", globals.node_HUD, "on_not_ready_ability")
	connect("not_ready_fire", globals.node_HUD, "on_not_ready_fire")
	connect("not_enough_ammo", globals.node_HUD, "on_not_enough_ammo")
	
	# Init Player
	player.is_local_player = true
	player.connect("ability_cd_start", self, "ability_cd_start")
	player.connect("died", globals.node_PlayerSelect, "show_select") # So can respawn
	player.connect("died", self, "player_on_died")
	player.connect("toggle_invis", self, "player_toggle_invis")
	player.connect("health_changed", globals.node_HUD, "update_health") # Should really store these node paths in some singleton
	player.connect("hit_border", globals.node_world.get_node("Border"), "hit_border")
	
	player.emit_signal("health_changed", player.health, player.MAX_HEALTH) # Init health in hud
	
	# Init Gun
	can_fire = true
	gun.connect("refresh", self, "gun_refresh")
	gun.connect("rearm", self, "gun_rearm")
	gun.connect("ammo_changed", globals.node_HUD, "ammo_changed")
	gun.emit_signal("ammo_changed", gun.bullet_count, gun.MAX_BULLETS)
	
	# Init Shield
	shield.connect("health_changed", globals.node_HUD, "shield_health_changed")
	shield.emit_signal("health_changed", shield.health, shield.MAX_HEALTH) # Init health in hud


func _draw():
	if player != null and ability_id == "1":
		if $AbilityCooldown.is_stopped():
			draw_circle(Vector2(), float(player.TELLEPORT_RANGE), Color(0, .5, 0, .3))
		else:
			pass # Turn off drawing


func _on_AbilityCooldown_timeout():
	emit_signal("ready_ability")


func ability_cd_start():
	$AbilityCooldown.start()
	
	if ability_id == "1":
		# Turn off visible target range
		update()


func _on_FireDelay_timeout():
	can_fire = true


func gun_refresh():
	can_fire = true
	$FireDelay.stop()


func gun_rearm():
	can_fire = false
	$FireDelay.start()


func _on_ability_pressed():
	if $AbilityCooldown.is_stopped():
		awaiting_telleport_click = true
		update()
	else:
		emit_signal("not_ready_ability")


func player_on_died():
	globals.node_HUD.hide() # Make HUD invisible once player is dead 
	globals.node_world.get_node("Camera2D").disconnect_from_target()
	
	player = null
	gun = null
	gun_anchor = null
	shield = null
	ability_id = "-1"

func player_toggle_invis(invis_on):
	player.visible = true
	if invis_on:
		player.modulate.a = .5
	else:
		player.modulate.a = 1


