extends KinematicBody2D

var SPEED_MOVE
var TELLEPORT_RANGE

var MAX_HEALTH
var health
var id = -1

signal died()
signal health_changed(new_health, max_health)
signal hit_border()
signal ability_cd_start()
signal toggle_invis(invis_on)

var dead = false
var is_local_player = false # Hack

var velocity = Vector2()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	move_and_collide(velocity * delta)
	
	if position.x < 0:
		position.x = 0
		emit_signal("hit_border")
	elif position.x > globals.BOUNDS.size.x:
		position.x = globals.BOUNDS.size.x
		emit_signal("hit_border")
	if position.y < 0:
		position.y = 0
		emit_signal("hit_border")
	elif position.y > globals.BOUNDS.size.y:
		position.y = globals.BOUNDS.size.y
		emit_signal("hit_border")


puppet func update_vals(_position, _velocity, _rotation):
	position = _position
	if not is_local_player:
		velocity = _velocity
		$GunAnchor.rotation = _rotation


puppet func update_movespeed(new_speed):
	SPEED_MOVE = new_speed


puppet func toggle_invis(invis_on):
	visible=!invis_on 
	emit_signal("toggle_invis", invis_on)


puppet func ability_cd_start():
	emit_signal("ability_cd_start")


func set_player_name(new_name):
	get_node("you").set_text(new_name)


func set_gun(gun, offset):
	if has_node("GunAnchor/Gun"):
		$GunAnchor/Gun.queue_free()
	
	gun.set_name("Gun")
	gun.position = offset
	$GunAnchor.add_child(gun)
#	move_child(gun, 0)


func set_shield(shield):
	if has_node("Shield"):
		$Shield.queue_free()
	
	shield.set_name("Shield")
	add_child(shield)
	move_child(shield, 0)


puppet func health_change(new_ammount):
	health = new_ammount
	update_health_display()


func update_health_display():
	$Face.modulate = Color.white.linear_interpolate(Color.red, 1 - health/MAX_HEALTH)
	
	emit_signal("health_changed", health, MAX_HEALTH)


puppet func die(by_who_id):
	emit_signal("died")
	
	queue_free() # Get rid of player on all peers

